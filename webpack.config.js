/* eslint-env node */
const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ModernizrWebpackPlugin = require('modernizr-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

const config = {
  entry: './js/script.js',
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  devtool: isProduction ? 'source-map' : 'cheap-module-eval-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.scss$|\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'resolve-url-loader',
          {
            loader: 'sass-loader',
            options: {
              // Source maps are required for resolve-url-loader
              sourceMap: TRUE,
            }
          },
        ]
      },
      {
        test: /\.(png|jpg|gif|svg|eot|woff2|woff|ttf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'assets/',
              publicPath: '../assets'
            },
          },
        ],
      },
      {
        test: require.resolve('jquery'),
        use: [
          {
            loader: 'expose-loader',
            options: 'jQuery'
          },
          {
              loader: 'expose-loader',
              options: '$'
          }
        ]
      },
      {
        test: require.resolve('wowjs'),
        use: [
          {
            loader: 'expose-loader',
            options: 'WOW'
          }
        ]
      }
    ],
  },
  plugins: [
    new CleanWebpackPlugin(['./dist']),
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
    }),
    // Build our own Modernizer so we can remove the .details class on the
    // <html> element.
    new ModernizrWebpackPlugin({
      filename: 'js/modernizr.js',
      minify: isProduction,
      classPrefix: "modernizr-",
      options: [
        "addTest",
        "prefixes",
        "testStyles"
      ],
      "feature-detects": [
        "elem/details",
        "inputtypes",
        "touchevents"
      ]
    }),
  ],
};

if (isProduction) {
  config.plugins.push(new UglifyJSPlugin({
    sourceMap: TRUE,
  }));
}

module.exports = config;
