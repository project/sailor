import 'jquery';
import 'jquery-ui/ui/widgets/datepicker';
import 'jquery.uniform/src/js/jquery.uniform';
import 'lightslider';
import 'lightgallery';
import 'lightgallery/modules/lg-autoplay';
import 'lightgallery/modules/lg-fullscreen';
import 'lightgallery/modules/lg-hash';
import 'lightgallery/modules/lg-thumbnail';
import 'lightgallery/modules/lg-zoom';
import '@themeenergy/sailor/js/jetmenu';
import '@themeenergy/sailor/js/scripts';
import '../scss/style.scss';

$(document).ready(function () {

  $('#gallery').lightGallery({
    download: FALSE,
    getCaptionFromTitleOrAlt: FALSE,
    selector: 'figure'
  });

});
