<?php

/**
 * @file
 * Sailor Theme.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Template\Attribute;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * Implements hook_theme().
 */
function sailor_theme() {
  return [
    'sailor_tab_navigation' => [
      'variables' => [
        'attributes' => NULL,
        'list' => NULL,
      ],
    ],
    'sailor_tab_navigation_item' => [
      'variables' => [
        'attributes' => NULL,
        'type' => NULL,
        'item' => NULL,
      ],
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_form_alter().
 *
 * Add a suggestion for the form id.
 */
function sailor_theme_suggestions_form_alter(array &$suggestions, array $variables) {
  if (!empty($variables['element']['#form_id'])) {
    $form_id = $variables['element']['#form_id'];

    if (strpos($form_id, 'contact_message') !== FALSE) {
      if ($form_id != 'contact_message_personal_form') {
        $suggestions[] = 'form__contact';
        return;
      }
    }

    if (strpos($form_id, 'comment') !== FALSE) {
      $routes = [
        'comment.reply',
        'entity.comment.edit_form',
      ];

      if (!in_array(\Drupal::routeMatch()->getRouteName(), $routes)) {
        return;
      }
    }
  }

  $suggestions[] = 'form__one_half_modal';
}

/**
 * Impelements hook_form_user_pass_alter().
 *
 * Add the class to the password email text.
 */
function sailor_form_user_pass_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['mail']['#prefix'] = '<p class="full-width">';
}

/**
 * Impelements hook_form_user_login_form_alter().
 *
 * Add help text and links.
 */
function sailor_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $pass = Url::fromRoute('user.pass');
  $register = Url::fromRoute('user.register');
  if ($pass->access() || $register->access()) {
    $form['links'] = [
      '#type' => 'container',
      '#weight' => 1000,
      '#attributes' => [
        'class' => [
          'full-width',
        ],
      ],
    ];

    if ($pass->access()) {
      $form['links']['pass'] = [
        '#type' => 'container',
        'link' => [
          '#type' => 'link',
          '#title' => t('Forgotten Password?'),
          '#url' => $pass,
        ],
      ];
    }

    if ($register->access()) {
      $form['links']['register'] = [
        '#type' => 'container',
        'text' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => t('Dont have an account yet?'),
        ],
        'link' => [
          '#type' => 'link',
          '#title' => t('Sign up'),
          '#url' => $register,
        ],
      ];
    }
  }
}

/**
 * Impelements hook_form_user_pass_reset_alter().
 *
 * Ensures that every item has the proper class.
 */
function sailor_form_user_pass_reset_alter(&$form, FormStateInterface $form_state, $form_id) {
  $form['wrapper'] = [
    '#type' => 'container',
    '#attributes' => [
      'class' => [
        'full-width',
      ],
    ],
  ];

  $form['wrapper']['message'] = $form['message'];
  $form['wrapper']['help'] = $form['help'];

  unset($form['message']);
  unset($form['help']);
}

/**
 * Impelements hook_form_user_register_form_alter().
 *
 * Add login links.
 */
function sailor_form_user_register_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $login = Url::fromRoute('user.login');
  if ($login->access()) {
    $form['login'] = [
      '#type' => 'container',
      '#weight' => 1000,
      '#attributes' => [
        'class' => [
          'full-width',
        ],
      ],
      'text' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => t('Already have an account?'),
      ],
      'space' => [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => ' ',
      ],
      'link' => [
        '#type' => 'link',
        '#title' => t('Login'),
        '#url' => $login,
      ],
    ];
  }
}

/**
 * Impelements hook_form_alter().
 *
 * Add classes to submit button.
 */
function sailor_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (!empty($form['actions']['submit'])) {
    $build_info = $form_state->getBuildInfo();

    $form['actions']['submit']['#attributes']['class'][] = 'button';
    $form['actions']['submit']['#attributes']['class'][] = 'gold';

    if (isset($build_info['base_form_id']) && $build_info['base_form_id'] == 'comment_form') {
      $form['actions']['submit']['#attributes']['class'][] = 'large';
    }
    else {
      $form['actions']['submit']['#attributes']['class'][] = 'medium';
      $form['actions']['submit']['#attributes']['class'][] = 'full';
    }
  }

  if (!empty($form['actions']['preview'])) {
    $form['actions']['preview']['#access'] = FALSE;
  }

  if (strpos($form['#form_id'], 'contact_message') !== FALSE) {
    $form['langcode']['#access'] = FALSE;
  }
}

/**
 * Impelements template_preprocess_form_element().
 *
 * Add the full-width class.
 */
function sailor_preprocess_form_element(&$variables) {
  if (isset($variables['element']['#parents']) && count($variables['element']['#parents']) == 1) {
    $variables['attributes']['class'][] = 'full-width';
  }
}

/**
 * Impelements template_preprocess_html().
 *
 * Add the proper body class.
 */
function sailor_preprocess_html(&$variables) {
  $request = \Drupal::request();
  if ($request->attributes->has('_route')) {
    $route_name = $request->attributes->get('_route');
    $pieces = explode('.', $route_name);
    if (isset($pieces[1]) && $request->attributes->has($pieces[1])) {
      $entity_type = $pieces[1];
      $entity = $request->attributes->get($entity_type);
      if ($entity instanceof ContentEntityInterface) {
        $storage = \Drupal::entityTypeManager()->getStorage('entity_view_display');
        if ($display = $storage->load($entity_type . '.' . $entity->bundle() . '.default')) {
          if ($settings = $display->getThirdPartySetting('ds', 'layout')) {
            if ($settings['id'] == 'sailor_home') {
              $variables['attributes']['class'][] = 'home';
            }
          }
        }
      }
    }
  }
}

/**
 * Impelements template_preprocess_container().
 *
 * Add the full-width class.
 */
function sailor_preprocess_container(&$variables) {
  if (!empty($variables['element']['administration_menu'])) {
    return;
  }
  if (!$variables['has_parent'] || $variables['element']['#type'] == 'actions' || !empty($variables['element']['widget'])) {
    $variables['attributes']['class'][] = 'full-width';
  }
}

/**
 * Implements hook_theme_suggestions_block_alter().
 */
function sailor_theme_suggestions_block_alter(array &$suggestions, array $variables) {
  $storage = \Drupal::entityTypeManager()->getStorage('block');
  $block = isset($variables['elements']['#id']) ? $storage->load($variables['elements']['#id']) : NULL;

  // Customize the Menus in the footer.
  if ($block && $block->getPlugin() && $block->getPlugin()->getBaseId() == 'system_menu_block') {
    if (in_array($block->getRegion(), sailor_footer_regions())) {
      $suggestions[] = 'block__system_menu_block__footer';
    }
  }
  // Customize all of the blocks in the footer.
  elseif ($block && in_array($block->getRegion(), sailor_footer_regions())) {
    $suggestions[] = 'block__footer';
  }
}

/**
 * Implements template_preprocess_block().
 */
function sailor_preprocess_block(array &$variables) {
  $storage = \Drupal::entityTypeManager()->getStorage('block');
  $block = isset($variables['elements']['#id']) ? $storage->load($variables['elements']['#id']) : NULL;

  // Only modify blocks that use theme menu.
  if (!empty($variables['content']['#theme'])) {
    if (is_string($variables['content']['#theme']) && strrpos($variables['content']['#theme'], 'menu') === 0) {

      // Add the `main-nav` and 'jetmenu' class(es) when block is placed in the
      // primary menu region.
      if ($block->getRegion() == 'primary_menu') {
        $variables['attributes']['class'][] = 'main-nav';

        $variables['content']['#attributes']['class'][] = 'jetmenu';
        $variables['content']['#attributes']['id'] = Html::getUniqueId('jetmenu');
      }

      // Alter the theme hook suggestions.
      $region = $block->getRegion();
      if (in_array($region, sailor_footer_regions())) {
        $region = 'footer';
      }

      if (is_string($variables['elements']['#configuration']['id']) && strrpos($variables['elements']['#configuration']['id'], ':') !== FALSE) {
        $pieces = explode(':', $variables['elements']['#configuration']['id']);
        $variables['content']['#theme'] = 'menu__' . $region . '__' . $pieces[1];
      }
      else {
        $variables['content']['#theme'] = $variables['content']['#theme'] . '__' . $region;
      }
    }
  }

}

/**
 * Adds background image to an element.
 */
function sailor_background_image(array &$variables) {
  $attributes = $variables['attributes'];
  $id = $attributes->toArray()['id'];

  // Get the image for the hero.
  $image = NULL;
  foreach (Element::children($variables['content']['background']) as $key) {
    if (!empty($variables['content']['background'][$key]['#field_type'])) {
      if ($variables['content']['background'][$key]['#field_type'] == 'image') {
        $image = $variables['content']['background'][$key];
        unset($variables['content']['background'][$key]);
        break;
      }
    }
  }

  // Get the Image URL.
  $url = NULL;
  if (!empty($image) && $file = $image[0]['#item']->entity) {
    if (isset($image[0]['#image_style'])) {
      $style = ImageStyle::load($image[0]['#image_style']);
      $url = $style->buildUrl($file->getFileUri());
    }
    elseif (isset($image[0]['#responsive_image_style_id'])) {
      $responsive = ResponsiveImageStyle::load($image[0]['#responsive_image_style_id']);
      $mappings = $responsive->getImageStyleMappings();
      $breakpoints = \Drupal::service('breakpoint.manager')->getBreakpointsByGroup($responsive->getBreakpointGroup());

      // CSS Styles need a different order than picture tags.
      usort($mappings, function ($a, $b) use ($breakpoints) {
        $a_m = (int) trim($a['multiplier'], 'x');
        $b_m = (int) trim($b['multiplier'], 'x');

        $breakpoint_id = $a['breakpoint_id'];
        $a_b = $breakpoints[$breakpoint_id];

        $breakpoint_id = $b['breakpoint_id'];
        $b_b = $breakpoints[$breakpoint_id];

        if ($a_b->getWeight() == $b_b->getWeight()) {
          if ($a_m == $b_m) {
              return 0;
          }

          return ($a_m < $b_m) ? -1 : 1;
        }

        return ($a_b->getWeight() < $b_b->getWeight()) ? -1 : 1;
      });

      $css = array_map(function ($data) use ($file, $breakpoints, $id) {
        $style = ImageStyle::load($data['image_mapping']);
        $url = $style->buildUrl($file->getFileUri());

        $multiplier = (int) trim($data['multiplier'], 'x');

        $breakpoint_id = $data['breakpoint_id'];
        $breakpoint = $breakpoints[$breakpoint_id];

        return '@media '
          . $breakpoint->getMediaQuery()
          . ' and (min-resolution: ' . $multiplier * 96 . 'dpi), '
          . $breakpoint->getMediaQuery()
          . ' and (-webkit-min-device-pixel-ratio: ' . $multiplier . ') { #'
          . $id
          . ' { background-image: url("' . \Drupal::service('file_url_generator')->transformRelative($url) . '"); '
          . '} }';
      }, $mappings);

      $variables['css'] = implode("\n", $css);
    }
    else {
      $url = $file->url();
    }
  }

  // Add the image as the background.
  if ($url) {
    // For some reason, an underscore in the variable name does not work.
    $attributes->setAttribute('style', 'background-image: url("' . \Drupal::service('file_url_generator')->transformRelative($url) . '");');
  }
}

/**
 * Implments template_preprocess_sailor_hero().
 */
function sailor_preprocess_sailor_hero(array &$variables) {
  $entity_type = $variables['content']['#entity_type'];
  $entity = $variables['content']['#entity'];
  $id = Html::getUniqueId($entity_type . '-' . $entity->id());
  $attributes = new Attribute([
    'id' => $id,
  ]);
  $variables['attributes'] = $attributes;
  $variables['css'] = '';

  sailor_background_image($variables);

  // Add the classes to the hero button.
  foreach (Element::children($variables['content']['main']) as $key) {
    if (!empty($variables['content']['main'][$key]['#formatter'])) {
      if ($variables['content']['main'][$key]['#formatter'] == 'link') {
        foreach (Element::children($variables['content']['main'][$key]) as $delta) {
          $item = &$variables['content']['main'][$key][$delta];
          $item['#attributes']['class'][] = 'anchor';
          $item['#attributes']['class'][] = 'button';
          $item['#attributes']['class'][] = 'white';
          $item['#attributes']['class'][] = 'medium';
          $item['#attributes']['class'][] = 'wow';
          $item['#attributes']['class'][] = 'fadeInUp';
        }
      }
    }
  }
}

/**
 * Implments template_preprocess_sailor_photo().
 */
function sailor_preprocess_sailor_photo(array &$variables) {
  $entity_type = $variables['content']['#entity_type'];
  $entity = $variables['content']['#entity'];
  $id = Html::getUniqueId($entity_type . '-' . $entity->id());
  $attributes = new Attribute([
    'id' => $id,
  ]);
  $variables['attributes'] = $attributes;
  $variables['css'] = '';

  sailor_background_image($variables);

  // Add the classes to the hero button.
  foreach (Element::children($variables['content']['button']) as $key) {
    if (!empty($variables['content']['button'][$key]['#formatter'])) {
      if ($variables['content']['button'][$key]['#formatter'] == 'link') {
        foreach (Element::children($variables['content']['button'][$key]) as $delta) {
          $item = &$variables['content']['button'][$key][$delta];
          $item['#attributes']['class'][] = 'button';
          $item['#attributes']['class'][] = 'white';
          $item['#attributes']['class'][] = 'medium';
        }
      }
    }
  }
}

/**
 * Implments template_preprocess_sailor_follow_block().
 *
 * Remove the title from link fields, since the title messes up the display of
 * the icons.
 */
function sailor_preprocess_sailor_follow_block(array &$variables) {
  if (!empty($variables['content']['links'])) {
    foreach ($variables['content']['links'] as &$field) {
      foreach (Element::children($field) as $delta) {
        $item = &$field[$delta];

        // Link Field.
        if (!empty($item['#type']) && $item['#type'] == 'link') {
          $item['#title'] = '';
          $item['#attributes']['class'][] = 'circle';
        }
      }
    }
  }
}

/**
 * Gets a class based on the number of items.
 */
function sailor_size_class(array $items) {
  $count = count($items);
  switch ($count) {
    case 1:
      return 'full-width';

    case 2:
      return 'one-half';

    case 3:
      return 'one-third';

    default:
      return 'one-fourth';

  }
}

/**
 * Implments template_preprocess_sailor_crew().
 *
 * Add size icons for field values in the crew section.
 */
function sailor_preprocess_sailor_crew(array &$variables) {
  if (!empty($variables['content']['main'])) {
    foreach ($variables['content']['main'] as &$field) {
      $children = Element::children($field);
      $class = sailor_size_class($children);

      foreach ($children as $delta) {
        $item = &$field[$delta];
        $item['#attributes']['class'][] = 'item';
        $item['#attributes']['class'][] = $class;
      }

    }
  }
}

/**
 * Implments template_preprocess_sailor_yacht_single().
 *
 * Add size icons for field values in the gallery section.
 */
function sailor_preprocess_sailor_yacht_single(array &$variables) {
  foreach (Element::children($variables['content']['gallery']) as $index) {
    $field = $variables['content']['gallery'][$index];
    $children = Element::children($field);
    $class = sailor_size_class($children);

    foreach ($children as $delta) {
      $variables['content']['gallery'][$index][$delta]['#attributes']['class'][] = $class;
    }
  }

  $nav = [
    '#theme' => 'sailor_tab_navigation',
    '#list' => [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [],
      '#attributes' => [
        'class' => 'wrap',
      ],
    ],
  ];

  foreach (Element::children($variables['content']['tabs']) as $delta) {
    $field = $variables['content']['tabs'][$delta];

    foreach (Element::children($field) as $index) {
      $item = $field[$index];

      if (isset($item['#pre_render'])) {
        foreach ($item['#pre_render'] as $callable) {
          $item = call_user_func($callable, $item);
        }
      }

      $label = [];
      if (isset($item['#ds_configuration']['regions']['label'])) {
        foreach ($item['#ds_configuration']['regions']['label'] as $f) {
          $label[$f] = $item[$f];
        }
      }

      $rendered = \Drupal::service('renderer')->renderPlain($label);
      $id = Html::getUniqueId(trim(strip_tags($rendered)));

      // strlen('sailor_yacht_single_') === 20.
      $type = substr($item['#ds_configuration']['layout']['id'], 20);
      $variables['content']['tabs'][$delta][$index]['#attributes']['id'] = $id;

      $nav['#list']['#items'][] = [
        'value' => [
          '#theme' => 'sailor_tab_navigation_item',
          '#attributes' => [
            'href' => '#' . $id,
          ],
          '#type' => $type,
          '#item' => $label,
        ],
      ];
    }
  }

  $variables['content']['nav'] = $nav;
}

/**
 * Implments template_preprocess_sailor_crew_member().
 *
 * Remove the title from link fields, since the title messes up the display of
 * the icons.
 */
function sailor_preprocess_sailor_crew_member(array &$variables) {
  if (!empty($variables['content']['contact'])) {
    foreach ($variables['content']['contact'] as &$field) {
      foreach (Element::children($field) as $delta) {
        $item = &$field[$delta];

        // Link Field.
        if (!empty($item['#type']) && $item['#type'] == 'link') {
          $item['#title'] = '';
        }
      }
    }
  }
}

/**
 * Implments template_preprocess_sailor_account().
 */
function sailor_preprocess_sailor_account(array &$variables) {
  if (!empty($variables['content']['intro'])) {
    foreach ($variables['content']['intro'] as &$field) {
      foreach (Element::children($field) as $delta) {
        $item = &$field[$delta];

        // Add the `profile-pic` class to the images in the intro.
        if (!empty($item['#theme']) && $item['#theme'] == 'image_formatter') {
          $item['#item_attributes']['class'][] = 'profile-pic';
        }
      }
    }
  }

  if (!empty($variables['content']['sidebar_left'])) {
    foreach ($variables['content']['sidebar_left'] as &$field) {
      foreach (Element::children($field) as $delta) {
        $item = &$field[$delta];

        // Change the theme of the sidebar "tabs".
        if (!empty($item[0]['#theme']) && $item[0]['#theme'] == 'menu_local_tasks') {
          $item[0]['#theme'] = 'menu_local_tasks__sidebar';
        }
      }
    }
  }
}

/**
 * Implments template_preprocess_error().
 *
 * Add the error image to the tempalte.
 */
function sailor_preprocess_sailor_error(array &$variables) {
  $variables['image'] = [
    '#theme' => 'image',
    '#uri' => \Drupal::service('extension.list.theme')->getPath('sailor') . '/images/ico3-gold.png',
  ];
}

/**
 * Implmenets template_preprocess_menu_local_tasks__sidebar().
 *
 * Add the 'current' class to the current menu item.
 */
function sailor_preprocess_menu_local_tasks__sidebar(array &$variables) {
  foreach (['primary', 'secondary'] as $tasks) {
    if (empty($variables[$tasks])) {
      continue;
    }

    $variables[$tasks] = array_map('sailor_task_current_class', $variables[$tasks]);
  }
}

/**
 * Implmeents template_preprocess_form__one_half_modal().
 *
 * Add the title as a variable for the form.
 */
function sailor_preprocess_form__one_half_modal(array &$variables) {
  $variables['title'] = sailor_get_title();
}

/**
 * Implmements template_preprocess_menu_local_tasks__tabs().
 *
 * Count number of tabs and add class.
 */
function sailor_preprocess_menu_local_tasks__tabs(&$variables) {
  $variables['attributes']['role'] = 'navigation';
  $variables['attributes']['class'][] = 'tabs';
  $variables['attributes']['class'][] = sailor_number_converter(count($variables['primary']));

  foreach ($variables['primary'] as &$link) {
    if (!$link['#active']) {
      continue;
    }

    $link['#attributes']['class'][] = 'current';
  }
}

/**
 * Implmements template_preprocess_sailor_services().
 */
function sailor_preprocess_sailor_services(&$variables) {
  $tabs = [
    '#theme' => 'sailor_tab_navigation',
    '#attributes' => [
      'class' => [
        'vertical',
      ],
    ],
    '#list' => [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [],
      '#attributes' => [
        'class' => 'wrap',
      ],
    ],
  ];

  foreach (Element::children($variables['content']['main']) as $delta) {
    $main = &$variables['content']['main'][$delta];

    foreach (Element::children($main) as $index) {
      $item = $main[$index];

      if (isset($item['#pre_render'])) {
        foreach ($item['#pre_render'] as $callable) {
          $item = call_user_func($callable, $item);
        }
      }

      $label = [];
      if (isset($item['#ds_configuration']['regions']['label'])) {
        foreach ($item['#ds_configuration']['regions']['label'] as $field) {
          $label[$field] = $item[$field];
        }
      }

      $rendered = \Drupal::service('renderer')->renderPlain($label);
      $id = Html::getUniqueId(trim(strip_tags($rendered)));

      $main[$index]['attributes']['id'] = $id;

      $tabs['#list']['#items'][] = [
        'value' => [
          '#theme' => 'sailor_tab_navigation_item',
          '#item' => $label,
          '#attributes' => [
            'href' => '#' . $id,
          ],
        ],
      ];
    }
  }

  array_unshift($variables['content']['sidebar'], $tabs);

}

/**
 * Implmements template_preprocess_sailor_call_to_action().
 *
 * Add the proper classes to the link(s).
 */
function sailor_preprocess_sailor_call_to_action(&$variables) {
  $hasMain = !sailor_region_is_empty($variables['content']['main']);
  $variables['hasMain'] = $hasMain;

  // Get the color(s) from the color position and add them as classes.
  if (!empty($variables['content']['color'])) {
    foreach (Element::children($variables['content']['color']) as $key) {
      if (empty($variables['content']['color'][$key]['#items'])) {
        continue;
      }

      $items = $variables['content']['color'][$key]['#items'];

      if (!$items instanceof TypedDataInterface) {
        continue;
      }

      foreach ($items as $item) {
        if (!$item instanceof TypedDataInterface) {
          continue;
        }

        $variables['attributes']['class'][] = Html::cleanCssIdentifier($item->getString());
      }
    }
  }

  if (!empty($variables['content']['title'])) {
    foreach (Element::children($variables['content']['title']) as $key) {
      $item = &$variables['content']['title'][$key];
      $item['#theme'] = 'field__cta_title';
      $item['#field_element'] = $hasMain ? 'h2' : 'h3';
    }
  }

  // Add the classes to the buttons.
  if (!empty($variables['content']['button'])) {
    foreach (Element::children($variables['content']['button']) as $key) {
      if ($variables['content']['button'][$key]['#field_type'] != 'link') {
        continue;
      }

      foreach (Element::children($variables['content']['button'][$key]) as $delta) {
        $classes = &$variables['content']['button'][$key][$delta]['#attributes']['class'];
        $classes[] = 'button';
        $classes[] = 'medium';
        $classes[] = 'white';

        if (!$hasMain) {
          $classes[] = 'right';
        }
      }
    }
  }
}

/**
 * Implmements template_preprocess_field__cta_title().
 *
 * Add the field element variable.
 */
function sailor_preprocess_field__cta_title(&$variables) {
  $variables['field_element'] = $variables['element']['#field_element'];
}

/**
 * Implmements template_preprocess_tab_set().
 *
 * Add the proper classes to the tab set.
 */
function sailor_preprocess_sailor_tab_set(&$variables) {
  $variables['attributes']['class'][] = 'tabs';
  $variables['attributes']['role'] = 'navigation';
  if (!empty($variables['content']['tabs'][0]['#items'])) {
    $items = $variables['content']['tabs'][0]['#items'];
    if ($items instanceof \Countable) {
      $variables['attributes']['class'][] = sailor_number_converter($items->count());
    }
  }
}

/**
 * Implmements template_preprocess_tab().
 *
 * Add the link to the tab.
 */
function sailor_preprocess_sailor_tab(&$variables) {
  // Remove the attributes from the images.
  foreach ($variables['content']['main'] as $data) {
    if (isset($data['#field_type']) && $data['#field_type'] == 'image') {
      foreach (Element::children($data) as $delta) {
        $image = $data[$delta]['#item'];
        $image->set('width', NULL);
        $image->set('height', NULL);
      }
    }
  }

  // Wrap all of main in the link.
  if (!empty($variables['content']['link'][0][0])) {
    if ($variables['content']['link'][0]['#field_type'] == 'link') {
      $link = $variables['content']['link'][0][0];
      $link['#title'] = $variables['content']['main'];
      $variables['content']['main'] = $link;
    }
  }
}

/**
 * Implmements template_preprocess_region().
 *
 * Add the region name as a class.
 */
function sailor_preprocess_region(&$variables) {
  $variables['attributes']['class'][] = 'region-' . $variables['region'];
}

/**
 * Converts a number to a string.
 *
 * @param int $number
 *   Number to be converted.
 *
 * @return string
 *   Number in string format.
 */
function sailor_number_converter($number) {
  $strings = sailor_number_strings();

  return $strings[$number] ?? '';
}

/**
 * Number strings.
 *
 * @return array
 *   An array of number strings.
 */
function sailor_number_strings() {
  return [
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
  ];
}

/**
 * Returns the Sailor Footer Regions.
 *
 * @return array
 *   Collection of regions that are in the "footer".
 */
function sailor_footer_regions() {
  return [
    'footer_first',
    'footer_second',
    'footer_third',
    'footer_fourth',
    'footer_copy',
  ];
}

/**
 * Add the 'current' class to every active link.
 *
 * @param array $task
 *   A task array.
 *
 * @return array
 *   A task array.
 *
 * @see array_map
 */
function sailor_task_current_class(array $task) {
  if (!empty($task['#active']) && $task['#active'] == TRUE) {
    $task['#attributes']['class'][] = 'current';
  }
  return $task;
}

/**
 * Sailor Get Page Title.
 *
 * @return string
 *   Title of the Page.
 */
function sailor_get_title() {
  $request = \Drupal::request();
  $route_match = \Drupal::routeMatch();
  return \Drupal::service('title_resolver')->getTitle($request, $route_match->getRouteObject());
}

/**
 * Implmements template_preprocess_sailor_call_to_action_small().
 *
 * Add the proper classes to the link(s).
 */
function sailor_preprocess_sailor_call_to_action_small(&$variables) {
  foreach (Element::children($variables['content']['main']) as $key) {
    if (!isset($variables['content']['main'][$key]['#field_type'])) {
      continue;
    }
    if ($variables['content']['main'][$key]['#field_type'] != 'link') {
      continue;
    }

    foreach (Element::children($variables['content']['main'][$key]) as $delta) {
      $variables['content']['main'][$key][$delta]['#attributes']['class'][] = 'trigger';
      $variables['content']['main'][$key][$delta]['#attributes']['class'][] = 'button';
      $variables['content']['main'][$key][$delta]['#attributes']['class'][] = 'full';
      $variables['content']['main'][$key][$delta]['#attributes']['class'][] = 'small';
      $variables['content']['main'][$key][$delta]['#attributes']['class'][] = 'white';
    }
  }
}

/**
 * Determine if a Display Suite Region is empty.
 */
function sailor_region_is_empty($region) {
  $rendered = \Drupal::service('renderer')->renderPlain($region);

  return empty(trim($rendered));
}

/**
 * Implments template_preprocess_sailor_gallery_image().
 */
function sailor_preprocess_sailor_gallery_image(array &$variables) {
  $entity = $variables['content']['#entity'];
  $variables['attributes']['href'] = $entity->url();
}

/**
 * Gets the type icon for tab navigation.
 */
function sailor_type_icon($type) {
  switch ($type) {
    case 'description':
      return 'icojam_info_3';

    case 'specifications':
      return 'icojam_document';

    case 'equipment':
      return 'icojam_anchor';

    case 'availability':
      return 'icojam_calendar_1';

    case 'contact_broker':
      return 'icojam_target';

    case 'brochure':
      return 'icojam_inbox_receive';

    default:
      return '';

  }
}

/**
 * Implments template_preprocess_sailor_tab_navigation().
 */
function sailor_preprocess_sailor_tab_navigation(array &$variables) {
  if (isset($variables['list']['#items'])) {
    $count = count($variables['list']['#items']);
    $variables['attributes']['class'][] = sailor_number_converter($count);
  }
}

/**
 * Implments template_preprocess_sailor_tab_navigation_item().
 */
function sailor_preprocess_sailor_tab_navigation_item(array &$variables) {
  if (!isset($variables['type'])) {
    return;
  }

  $variables['icon'] = sailor_type_icon($variables['type']);
}

/**
 * Implments template_preprocess_sailor_yacht_single_availability().
 */
function sailor_preprocess_sailor_yacht_single_availability(array &$variables) {
  foreach (['left', 'right'] as $region) {
    foreach (Element::children($variables['content'][$region]) as $field) {
      if (!isset($variables['content'][$region][$field]['#field_type'])) {
        continue;
      }

      if ($variables['content'][$region][$field]['#field_type'] !== 'link') {
        continue;
      }

      foreach (Element::children($variables['content'][$region][$field]) as $delta) {
        if (!isset($variables['content'][$region][$field][$delta]['#type'])) {
          continue;
        }

        if ($variables['content'][$region][$field][$delta]['#type'] !== 'link') {
          continue;
        }

        $class = $variables['content'][$region][$field][$delta]['#attributes']['class'] ?? [];

        // Add the classes to any link fields.
        $variables['content'][$region][$field][$delta]['#attributes']['class'] = $class + [
          'button',
          'gold',
          'large',
          'full',
        ];
      }
    }
  }
}

/**
 * Implments template_preprocess_sailor_yacht_single_availability().
 */
function sailor_preprocess_sailor_deal(array &$variables) {
  foreach (Element::children($variables['content']['button']) as $field) {
    if (!isset($variables['content']['button'][$field]['#field_type'])) {
      continue;
    }

    if ($variables['content']['button'][$field]['#field_type'] !== 'link') {
      continue;
    }

    foreach (Element::children($variables['content']['button'][$field]) as $delta) {
      if (!isset($variables['content']['button'][$field][$delta]['#type'])) {
        continue;
      }

      if ($variables['content']['button'][$field][$delta]['#type'] !== 'link') {
        continue;
      }

      $class = $variables['content']['button'][$field][$delta]['#attributes']['class'] ?? [];

      // Add the classes to any link fields.
      $variables['content']['button'][$field][$delta]['#attributes']['class'] = $class + [
        'button',
        'gold',
        'medium',
        'full',
        'solid',
      ];
    }
  }
}
