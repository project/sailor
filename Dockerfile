FROM drupal:8

# Disable OpCache for Development.
RUN { \
		echo 'opcache.enable=0'; \
} > /usr/local/etc/php/conf.d/opcache-recommended.ini

COPY ./ /var/www/html/themes/sailor
