INTRODUCTION
------------
Sailor is a Drupal port of the Sailor HTML Template
(https://themeforest.net/item/sailor-yacht-charter-booking-html-template/10868502)
 * For a full description of the theme, visit the project page:
   https://drupal.org/project/sailor
 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/sailor

REQUIREMENTS
------------
This theme requires a license to Sailor, which can be purchased at:
https://themeforest.net/item/sailor-yacht-charter-booking-html-template/10868502

You will also need to obtain an Envato Market token, with the "Download your
purchased items" permission. This can be obtained at:
https://build.envato.com/create-token/

You will also need Node.js & NPM to build the theme's dependencies.

RECOMMENDED MODULES
-------------------
 * Display Suite (https://www.drupal.org/project/ds):
   When enabled, allows you to use different templates on a per-entity bundle
   basis. Also allows you to customize where non-fields (i.e. Title) should go.
 * Paragraphs (https://www.drupal.org/project/paragraphs):
   Templates in this theme are designed to be used with entities. Paragraphs
   gives you a way to create multiple entities on the page (as is the case with
   the homepage template).

INSTALLATION
------------
  * Install with Composer as documented here:
    https://www.drupal.org/docs/develop/using-composer/using-composer-to-manage-drupal-site-dependencies

    The command will be something like:
    composer require drupal/sailor

    Composer will also then execute NPM. You will be asked for your Envato
    Market token. Or it can be added as an environment variable with the name:
    ENVATO_TOKEN

  * Enable the theme like any other theme.

CONFIGURATION
-------------
  * Configure the block placements as the names of the placements are different
    then the core themes.
  * If Field UI is enabled, go to the "Manage Display" page of any entity,
    select the layout at the bottom and customize the field positions.

MAINTAINERS
-----------
Current maintainers:
 * David Barratt (davidwbarratt) - https://www.drupal.org/u/davidwbarratt

This project has been sponsored by:
 * Sail Venice - https://www.drupal.org/sail-venice
